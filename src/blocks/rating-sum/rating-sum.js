const addStarGradient = (container, rating) => {
  container.forEach((star, i) => {
    if (i < Math.floor(rating)) star.style.setProperty('--gradient-stop', '100%');
    else if (i === Math.floor(rating)) star.style.setProperty('--gradient-stop', `${(rating - Math.floor(rating)) * 100}%`);
  });
};

const ratingInit = () => {
  const ratingSum = document.querySelector('.rating-sum');
  if (ratingSum) {
    const stars = Array.from(ratingSum.querySelectorAll('.rating-sum__star'));
    const ratingCount = ratingSum.querySelector('.rating-sum__count');
    ratingCount.innerHTML = ratingSum.dataset.rating;
    addStarGradient(stars, ratingSum.dataset.rating);
  }
};

export default ratingInit;
