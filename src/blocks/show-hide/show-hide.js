export default function showHide() {
  const showHideBlocks = Array.from(document.querySelectorAll('.show-hide'));
  showHideBlocks.forEach((item) => {
    const itemTitle = item.firstElementChild;
    itemTitle.addEventListener('click', (e) => {
      e.currentTarget.nextElementSibling.classList.toggle('show-hide__body--active');
    });
    return item;
  });
}
