const dropDown = () => {
  const items = document.querySelectorAll('.drop-down');
  if (!items) return;
  items.forEach((item) => {
    item.addEventListener('click', (event) => {
      const arrow = item.querySelector('.drop-down__arrow');
      const arrowAnimateOpen = arrow.querySelector('#open');
      const arrowAnimateClose = arrow.querySelector('#close');
      if (event.target.closest('.drop-down__header')) item.classList.toggle('drop-down--active');
      if (item.classList.contains('drop-down--active')) arrowAnimateOpen.beginElement();
      else arrowAnimateClose.beginElement();
    });
  });
};

export default dropDown;
